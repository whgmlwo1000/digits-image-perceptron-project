﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digits_Image_Perceptron_Project
{
    class MultipleLayerPerceptron
    {
        public float[] Inputs { get { return mLayers[0].Outputs; } }
        public float[] Outputs { get { return mLayers[mNumOfLayers - 1].Outputs; } }

        private ALayer[] mLayers;
        private int mNumOfLayers;


        public MultipleLayerPerceptron(int numOfLayer)
        {
            mNumOfLayers = numOfLayer;
            mLayers = new ALayer[mNumOfLayers];
        }

        public void SetLayer(int index, ALayer layer)
        {
            mLayers[index] = layer;
        }

        /* 추가됨 */
        public ALayer[] GetLayers()
        {
            return mLayers;
        }

        /* 추가됨 */
        public int GetCountOfLayer()
        {
            return mNumOfLayers;
        }

        public float[] Execute()
        {
            for(int i=1; i<mNumOfLayers; i++)
            {
                mLayers[i].Simulate(mLayers[i - 1]);
            }

            return Outputs;
        }
    }
}
