﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digits_Image_Perceptron_Project
{
    /// <summary>
    /// MLP 객체를 학습시키는 클래스
    /// </summary>
    class LayerTrainer
    {
        private MultipleLayerPerceptron mPerceptron;
        private ALayer[] mLayers;
        private SubLayer[] mSubLayers;
        private float mMomentum;
        private float mTrainRatio;

        private float mSign;

        public LayerTrainer(MultipleLayerPerceptron perceptron, float trainRatio, float momentum, bool mIsMaximum)
        {
            mPerceptron = perceptron;
            mSubLayers = new SubLayer[perceptron.GetCountOfLayer()];
            mMomentum = momentum;
            mTrainRatio = trainRatio;
            mSign = (mIsMaximum ? 1.0f : -1.0f);

            mLayers = mPerceptron.GetLayers();
            for (int i = 0; i < mSubLayers.Length; i++)
            {
                mSubLayers[i] = new SubLayer(mLayers[i].NumOfUnits, mLayers[i].NumOfPrevUnits);
            }
        }

        /// <summary>
        /// MLP 객체를 학습시키는 메소드
        /// 반드시 MLP 객체를 실행 시킨 뒤, 본 메소드를 호출해야 한다.
        /// </summary>
        /// <param name="expectedResult"></param>
        public void Train(float[] expectedResult)
        {
            int indexOfLastLayer = mLayers.Length - 1;
            // 출력 레이어 역전파 시작
            for (int j = 0; j < mLayers[indexOfLastLayer].NumOfUnits; j++)
            {
                // 현재 레이어의 각 유닛에 대한 델타값 저장
                mSubLayers[indexOfLastLayer].Deltas[j] = -(expectedResult[j] - mLayers[indexOfLastLayer].Outputs[j])
                    * mLayers[indexOfLastLayer].DeltaActivate(mLayers[indexOfLastLayer].Totals[j]);
                // 이전 레이어의 모든 유닛에 대해
                for (int i = 0; i < mLayers[indexOfLastLayer - 1].NumOfUnits; i++)
                {
                    float prevDeltaWeight = mSubLayers[indexOfLastLayer].GetDeltaWeight(i, j);
                    // 가중치 변화량 저장
                    mSubLayers[indexOfLastLayer].SetDeltaWeight(i, j,
                        mMomentum * prevDeltaWeight + mTrainRatio * mSubLayers[indexOfLastLayer].Deltas[j] * mLayers[indexOfLastLayer - 1].Outputs[i]);
                    // 기존 가중치값 저장 (이후의 학습을 위해서)
                    mSubLayers[indexOfLastLayer].SetOriginWeight(i, j, mLayers[indexOfLastLayer].GetWeight(i, j));

                    //float prevWeight = mLayers[indexOfLastLayer].GetWeight(i, j);

                    // 가중치값 갱신
                    mLayers[indexOfLastLayer].SetWeight(i, j, mLayers[indexOfLastLayer].GetWeight(i, j)
                        + mSign * mSubLayers[indexOfLastLayer].GetDeltaWeight(i, j));

                    //Console.WriteLine("After[Weight({0}, {1})] : {2}", i, j, mLayers[indexOfLastLayer].GetWeight(i, j) - prevWeight);
                }
            }

            for (int indexOfLayers = mLayers.Length - 2; indexOfLayers >= 1; indexOfLayers--)
            {
                for (int j = 0; j < mLayers[indexOfLayers].NumOfUnits; j++)
                {
                    mSubLayers[indexOfLayers].Deltas[j] = 0.0f;

                    for (int k=0; k<mLayers[indexOfLayers + 1].NumOfUnits; k++)
                    {
                        mSubLayers[indexOfLayers].Deltas[j] += mSubLayers[indexOfLayers + 1].Deltas[k] * mSubLayers[indexOfLayers + 1].GetOriginWeight(j, k);
                    }
                    
                    mSubLayers[indexOfLayers].Deltas[j] *= mLayers[indexOfLayers].DeltaActivate(mLayers[indexOfLayers].Totals[j]);

                    for(int i = 0; i<mLayers[indexOfLayers - 1].NumOfUnits; i++)
                    {
                        float prevDeltaWeight = mSubLayers[indexOfLayers].GetDeltaWeight(i, j);
                        mSubLayers[indexOfLayers].SetDeltaWeight(i, j,
                            mMomentum * prevDeltaWeight + mTrainRatio * mSubLayers[indexOfLayers].Deltas[j] * mLayers[indexOfLayers - 1].Outputs[i]);
                        mSubLayers[indexOfLayers].SetOriginWeight(i, j, mLayers[indexOfLayers].GetWeight(i, j));

                        //float prevWeight = mLayers[indexOfLayers].GetWeight(i, j);

                        mLayers[indexOfLayers].SetWeight(i, j, mLayers[indexOfLayers].GetWeight(i, j)
                           + mSign * mSubLayers[indexOfLayers].GetDeltaWeight(i, j));

                        //Console.WriteLine("After[Weight({0}, {1})] : {2}", i, j, mLayers[indexOfLayers].GetWeight(i, j) - prevWeight);
                    }
                }
            }
        }

    }

    /// <summary>
    /// MLP 객체를 학습시키기 위해 필요한 추가 필드 및 메소드 클래스
    /// </summary>
    class SubLayer
    {
        public float[] Deltas { get { return mDeltas; } }

        private float[] mDeltas;
        private float[] mDeltaWeights;
        private float[] mOriginWeights;
        private int mNumOfUnits;
        private int mNumOfPrevUnits;

        public SubLayer(int numOfUnits, int numOfPrevUnits)
        {
            mNumOfUnits = numOfUnits;
            mNumOfPrevUnits = numOfPrevUnits;
            mDeltas = new float[mNumOfUnits];
            mDeltaWeights = new float[mNumOfUnits * mNumOfPrevUnits];
            mOriginWeights = new float[mNumOfUnits * mNumOfPrevUnits];
        }

        public float GetDeltaWeight(int indexOfPrevUnits, int indexOfUnits)
        {
            return mDeltaWeights[indexOfPrevUnits + indexOfUnits * mNumOfPrevUnits];
        }
        public void SetDeltaWeight(int indexOfPrevUnits, int indexOfUnits, float value)
        {
            mDeltaWeights[indexOfPrevUnits + indexOfUnits * mNumOfPrevUnits] = value;
        }
        public float GetOriginWeight(int indexOfPrevUnits, int indexOfUnits)
        {
            return mOriginWeights[indexOfPrevUnits + indexOfUnits * mNumOfPrevUnits];
        }
        public void SetOriginWeight(int indexOfPrevUnits, int indexOfUnits, float value)
        {
            mOriginWeights[indexOfPrevUnits + indexOfUnits * mNumOfPrevUnits] = value;
        }
    }
}
