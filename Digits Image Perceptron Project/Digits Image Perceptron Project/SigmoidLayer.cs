﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digits_Image_Perceptron_Project
{
    class SigmoidLayer : ALayer
    {
        public SigmoidLayer(int numOfPrevUnits, int numOfUnits) : base(numOfPrevUnits, numOfUnits)
        {
            
        }

        public SigmoidLayer(int numOfPrevUnits, int numOfUnits, DataControler dataControler) : base(numOfPrevUnits, numOfUnits, dataControler)
        {

        }

        public override float Activate(float input)
        {
            return 1.0f / (1.0f + (float)Math.Exp(-input));
        }
        public override float DeltaActivate(float input)
        {
            return Activate(input) * (1.0f - Activate(input));
        }
    }
}
