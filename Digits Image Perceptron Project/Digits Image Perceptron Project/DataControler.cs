﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Digits_Image_Perceptron_Project
{
    //json 형식을 위한 클래스
    public class InfoOfTrainedData
    {
        public int mNumOfLayer { get; set; }
        public int[] mNumOfUnit { get; set; }
        public float[] mWeightOfFirst { get; set; }
        public float[] mWeifhtOfSecond { get; set; }
    }

    class DataControler
    {
        //불러올 파일 이름
        private string filepath;
        private string filename;
        //가중치 값들
        private float[] mWeightOfFirst;
        private float[] mWeightOfSecond;
        //레이어의 수
        private int mNumOfLayer;
        //각 레이어의 유닛 개수
        private int[] mNumOfUnit;

        public DataControler(string filepath)
        {
            this.filepath = filepath;
            this.filename = @"\InfoOfTrainedData.json";
        }

        public int GetNumOfLayer()
        {
            return mNumOfLayer;
        }

        public float[] GetWeightOfFirst()
        {
            return mWeightOfFirst;
        }

        public float[] GetWeightOfSecond()
        {
            return mWeightOfSecond;
        }

        public void LoadData()
        {
            /*
             1. 클래스의 filename의 이름으로된 파일 불러오기.
             2. 레이어 수와 각 레이어 유닛 개수를 변수에 대입.
             3. 각 weight배열들에 해당하는 값을 대입.
             */

            string jsonString = System.IO.File.ReadAllText(filepath + filename);
            
            InfoOfTrainedData data = new InfoOfTrainedData();
            data = JsonConvert.DeserializeObject<InfoOfTrainedData>(jsonString);

            mNumOfLayer = data.mNumOfLayer;
            mNumOfUnit = data.mNumOfUnit;
            mWeightOfFirst = data.mWeightOfFirst;
            mWeightOfSecond = data.mWeifhtOfSecond;

        }

        public void SaveData(MultipleLayerPerceptron perceptron)
        {
            /* 
             1. 레이어의 수와 각 레이어의 유닛 개수(mNumOfUnit[]) 을 저장.
             2. weight 값 저장.
             3. 대입된 데이터들을 가지고 클래스의 filename의 이름으로 파일 저장.
             */

            mNumOfLayer = perceptron.GetCountOfLayer();
            ALayer[] tempLayers = perceptron.GetLayers();
            mNumOfUnit = new int[mNumOfLayer];

            for(int i=0; i<mNumOfLayer; i++)
            {
                mNumOfUnit[i] = tempLayers[i].NumOfUnits;
            }//for i

            for (int i=1; i<mNumOfLayer; i++)
            {
                if (i == 1)
                {         
                    //첫번째 레이어의 유닛 개수와 두번째 레이어의 유닛 개수를 곱한 값 만큼 할당
                    mWeightOfFirst = new float[mNumOfUnit[i - 1] * mNumOfUnit[i]];
                    //통째로 가중치값 놓기
                    mWeightOfFirst = tempLayers[i].GetWeights();
                }
                else if (i == 2)
                {    
                    //두번째 레이어의 유닛 개수와 세번째 레이어의 유닛 개수를 곱한 값 만큼 할당
                    mWeightOfSecond = new float[mNumOfUnit[i - 1] * mNumOfUnit[i]];
                    //통째로 가중치값 놓기
                    mWeightOfSecond = tempLayers[i].GetWeights();
                }
            }//for i

            //형식에 맞게 데이터 삽입
            InfoOfTrainedData data = new InfoOfTrainedData();
            data.mNumOfLayer = mNumOfLayer;
            data.mNumOfUnit = mNumOfUnit;
            data.mWeightOfFirst = mWeightOfFirst;
            data.mWeifhtOfSecond = mWeightOfSecond;
            
            string jsonString = JsonConvert.SerializeObject(data);

            System.IO.File.WriteAllText(filepath + filename, jsonString);
        }

    }
}
