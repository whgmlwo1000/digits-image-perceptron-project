﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Digits_Image_Perceptron_Project
{
    class ImageDataSet
    {
        public readonly string DEFAULT_PATH = "./DataSet/";

        public int NumOfImages { get { return mNumOfImages; } }
        public int NumOfRows { get { return mNumOfRows; } }
        public int NumOfColumns { get { return mNumOfColumns; } }
        public byte[] Labels { get { return mLabels; } }
        public byte[][,] Images { get { return mImages;} }



        private int mNumOfImages;
        private int mNumOfRows;
        private int mNumOfColumns;

        private byte[] mLabels;
        private byte[][,] mImages;

        public ImageDataSet(string labelFileName, string imageFileName)
        {
            FileStream labelStream = new FileStream(DEFAULT_PATH + labelFileName, FileMode.Open);
            FileStream imageStream = new FileStream(DEFAULT_PATH + imageFileName, FileMode.Open);

            byte[] readBytes = new byte[4];
            byte[] integerBytes = new byte[4];
            byte[] oneByte = new byte[1];

            // Label File has Magic Number of dataset and the Number of labels from 0 to 7 offset
            labelStream.Seek(8, SeekOrigin.Begin);
            // Image File has Magic Number of dataset from 0 to 3 offset
            imageStream.Seek(4, SeekOrigin.Begin);
            // Image File has the Number of images from 4 to 7 offset
            imageStream.Read(readBytes, 0, 4);
            ReverseBytes(readBytes, integerBytes);
            mNumOfImages = BitConverter.ToInt32(integerBytes, 0);
            // Image File has the size of rows in a image from 8 to 11 offset
            imageStream.Read(readBytes, 0, 4);
            ReverseBytes(readBytes, integerBytes);
            mNumOfRows = BitConverter.ToInt32(integerBytes, 0);
            // Image File has the size of columns in a image from 12 to 15 offset
            imageStream.Read(readBytes, 0, 4);
            ReverseBytes(readBytes, integerBytes);
            mNumOfColumns = BitConverter.ToInt32(integerBytes, 0);

            mImages = new byte[mNumOfImages][,];
            mLabels = new byte[mNumOfImages];

            // After those offset in files, only labels and pixels are existed.
            for(int i=0; i<mNumOfImages; i++)
            {
                mImages[i] = new byte[mNumOfRows, mNumOfColumns];
                
                labelStream.Read(oneByte, 0, 1);
                mLabels[i] = oneByte[0];

                for (int j = 0; j < mNumOfRows; j++)
                {
                    for(int k = 0; k < mNumOfColumns; k++)
                    {
                        imageStream.Read(oneByte, 0, 1);
                        mImages[i][j, k] = oneByte[0];
                    }
                }
            }

            labelStream.Close();
            imageStream.Close();
        }

        private void ReverseBytes(byte[] srcBytes, byte[] dstBytes)
        {
            for(int i=srcBytes.Length - 1; i >= 0; i--)
            {
                dstBytes[srcBytes.Length - 1 - i] = srcBytes[i];
            }
        }
    }
}
